package notifications

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/mroth/sseserver"
)

// SSEServer holds the server instance
var SSEServer *sseserver.Server

// Start initializes and starts the SSE server
func Start(addr string) {
	fmt.Println("Starting SSE server...")
	SSEServer = sseserver.NewServer()

	// go serveTime()
	fmt.Printf("[SSE] serving on %s\n", addr)
	SSEServer.Serve(addr)
}

// SendSSE a message to SSE channel
func SendSSE(event string, msg interface{}, namespace string) {
	SSEServer.Broadcast <- sseserver.SSEMessage{Event: event, Data: ToBytes(msg), Namespace: namespace}
	// fmt.Printf("[SSE] sent message: %v\n", msg)
}

// ToBytes converts any object to bytes
func ToBytes(i interface{}) []byte {
	data, err := json.Marshal(i)
	if err != nil {
		fmt.Println(err)
		return []byte{}
	}

	return data
}

func serveTime() {
	for {
		time.Sleep(time.Second * 5)
		t := time.Now()
		msg := map[string]string{"time": t.Format(time.Stamp)}
		fmt.Printf("[SSE] sending time:\t%s\n", t.Format(time.Stamp))
		SendSSE("time-event", &msg, "/time")
	}
}
