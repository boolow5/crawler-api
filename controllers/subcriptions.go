package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/db"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/models"
	"github.com/gin-gonic/gin"
)

// AddSubscription creates subscription channel for future notifications
func AddSubscription(c *gin.Context) {
	form := struct {
		Name     string `json:"name" bson:"name"`
		Category string `json:"category" bson:"category"`
	}{}
	err := c.Bind(&form)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	var channel models.Subscription
	channel.Name = form.Name
	channel.Category = form.Category
	saved := false
	exists := false
	err = channel.Save()
	if err == nil {
		saved = err == nil
	} else if db.IsDupError(err) {
		exists = true
	} else {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"saved": saved, "exists": exists, "item": channel})
}

// GetSubscriptions finds all channels
func GetSubscriptions(c *gin.Context) {
	channels, err := models.GetSubscriptions()
	if err != nil {
		ErrorResponse(c, http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"channels": channels})
}

// SearchSubscriptions finds all channels
func SearchSubscriptions(c *gin.Context) {
	q := c.Query("q")
	category := c.Query("category")
	channels, err := models.SearchSubscriptions(q, category)
	if err != nil {
		ErrorResponse(c, http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"channels": channels})
}
