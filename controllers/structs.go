package controllers

// SocialAuthRequest is helper for binding social login request body
type SocialAuthRequest struct {
	Provider     string `json:"provider"`
	AccessToken  string `json:"access_token"`
	RequestToken string `json:"request_token"`
}
