package controllers

import "github.com/gin-gonic/gin"

// ErrorMessage is convinient way to prepare error mesage for json response
type ErrorMessage struct {
	Message string `json:"msg"`
}

// ErrorResponse generates error message easily
func ErrorResponse(c *gin.Context, status int, err error) {
	msg := ErrorMessage{
		Message: err.Error(),
	}

	c.JSON(status, msg)
	c.AbortWithError(status, err)
}
