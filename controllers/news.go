package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/models"
	"github.com/gin-gonic/gin"
)

// GetLatestNewsItems fetches latest news items from page until the page size limit
func GetLatestNewsItems(c *gin.Context) {
	page, _ := strconv.Atoi(c.Query("page"))
	limit, _ := strconv.Atoi(c.Query("limit"))
	category := c.Query("category")
	counter := models.CountItems(models.ClNewsItem, bson.M{})
	if counter < 0 {
		ErrorResponse(c, http.StatusInternalServerError, fmt.Errorf("No items in %s collection", models.ClNewsItem))
		return
	}

	if counter == 0 {
		ErrorResponse(c, http.StatusInternalServerError, errors.New("No news items found"))
		return
	}

	if page != 0 {
		page--
	}

	if limit == 0 {
		limit = 50
	}

	// if page number is more than max pages available
	if (counter / limit) < page {
		page = 0
	}

	// if the items available are less than to be divided to pages set page to 1
	if counter < limit {
		page = 0 // 1
	}

	if category == "" {
		category = "all"
	}

	items, err := models.GetNewsItems(page, limit, category)
	if err != nil {
		ErrorResponse(c, http.StatusInternalServerError, err)
		return
	}

	c.JSON(200, gin.H{
		"total":     counter,
		"page":      page + 1,
		"page_size": limit,
		"items":     items,
	})
}

// GetCategories finds all available categories
func GetCategories(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"categories": models.GetAllCategories()})
}

// SearchNewsItems finds news items by search query
func SearchNewsItems(c *gin.Context) {
	q := c.Query("q")
	page, _ := strconv.Atoi(c.Query("page"))
	limit, _ := strconv.Atoi(c.Query("limit"))
	category := c.Query("category")
	counter := models.CountItems(models.ClNewsItem, bson.M{})
	if counter < 0 {
		ErrorResponse(c, http.StatusInternalServerError, fmt.Errorf("No items in %s collection", models.ClNewsItem))
		return
	}

	if counter == 0 {
		ErrorResponse(c, http.StatusInternalServerError, errors.New("No news items found"))
		return
	}

	if page != 0 {
		page--
	}

	if limit == 0 {
		limit = 50
	}

	// if page number is more than max pages available
	if (counter / limit) < page {
		page = 1
	}

	// if the items available are less than to be divided to pages set page to 1
	if counter < limit {
		page = 1
	}

	if category == "" {
		category = "all"
	}
	items, err := models.SearchNewsItems(page, limit, category, q)
	if err != nil {
		ErrorResponse(c, http.StatusInternalServerError, err)
		return
	}

	c.JSON(200, gin.H{
		"total":     counter,
		"page":      page + 1,
		"page_size": limit,
		"items":     items,
	})
}
