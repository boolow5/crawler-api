package controllers

import (
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/models"
	"github.com/asaskevich/govalidator"
	"github.com/gin-gonic/gin"
)

// Signup is controller that saves user to database
func Signup(c *gin.Context) {
	user := models.User{}
	err := c.BindJSON(&user)
	if err != nil {
		fmt.Println("c.BindJSON() ERROR")
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	_, err = govalidator.ValidateStruct(user)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}

	exists, err := user.Exists()

	if err != nil {
		fmt.Println("user.Exists() ERROR")
		ErrorResponse(c, http.StatusInternalServerError, err)
		return
	}

	if exists {
		ErrorResponse(c, http.StatusBadRequest, errors.New("Email already exists"))
		return
	}

	if err := user.Save(); err != nil {
		fmt.Println("user.Save() ERROR")
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(200, gin.H{
		"user": user.Response(),
	})
}

// GetProfile gets the data for the current user
func GetProfile(c *gin.Context) {
	user := c.MustGet("user").(*models.User)

	if user.ID.Hex() == "" {
		ErrorResponse(c, http.StatusBadRequest, fmt.Errorf("Invalid user id = %s", user.ID.Hex()))
	}
	fmt.Println("USER:\t", user)
	c.JSON(http.StatusOK, gin.H{
		"user": user,
	})
}

// LoginUser authenticates user by email and password
func LoginUser(c *gin.Context) {
	input := models.User{}
	err := c.BindJSON(&input)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	pass := input.Password
	user, err := models.GetUserByEmail(input.Email)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	ok, err := user.Authenticate(pass)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	if !ok {
		err = errors.New("Incorrect email or password")
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(200, gin.H{"user": user.Response()})
}
