PKGNAME := news-crawler-api.tar.gz
COMMIT_NO := $(shell git rev-parse --short HEAD)
BUID_DATE := $(shell date -u +%Y-%m-%d_%H:%M:%S_%Z)
VERSION_NO := 1.0.1

build: linux-build package

linux-build:
	GOOS=linux GOARCH=amd64 go build -v -ldflags "-X bitbucket.org/boolow5/NewsCrawler/crawler-api/models.CommitNo=${COMMIT_NO} -X bitbucket.org/boolow5/NewsCrawler/crawler-api/models.LastBuidDate=${BUID_DATE} -X bitbucket.org/boolow5/NewsCrawler/crawler-api/models.Version=${VERSION_NO}" -o bin/news-crawler-api

clean:
	rm -r bin/*

init:
	mkdir -p bin log
	go get -v -u ./...

package:
	@mv bin old_bin;
	@mkdir -p bin;
	# @mkdir -p bin/templates
	@cp old_bin/news-crawler-api bin/;
	# @cp config.json bin/;
	# @cp templates/* bin/templates
	@cp -r data bin/;

	@tar -zcf ${PKGNAME} bin/;
	@rm -r bin;
	@mv old_bin bin;
	@echo "Package ready!"

# make deploy deploys test api
deploy:
	@bash deploy.sh test
	@echo "Deployed test successfully!"

# while make deploy-prod deploys production API
deploy-prod: # production
	@bash deploy.sh prod
	@echo "Deployed production successfully!"

# it's same as running 'make deploy && make deploy prod
deploy-both: deploy deploy-prod
