package routers

import (
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/controllers"
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/middlewares"
	"github.com/gin-gonic/gin"
)

// Init creates the router
func Init() *gin.Engine {
	r := gin.Default()
	r.Use(middlewares.Cors())
	api := r.Group("/api")
	api.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"msg": "Welcome to iWeydi!",
		})
	})
	api.GET("/version", controllers.GetVersion)
	api.GET("/ping", controllers.Ping)
	v1 := api.Group("/v1")
	{
		v1.POST("/signup", controllers.Signup)
		v1.POST("/signin", controllers.LoginUser)
		v1.GET("/news/categories", controllers.GetCategories)
		v1.GET("/news/latest", controllers.GetLatestNewsItems)
		v1.GET("/news/search", controllers.SearchNewsItems)
		v1.GET("/subscriptions", controllers.GetSubscriptions)
		v1.POST("/subscriptions", controllers.AddSubscription)
		v1.GET("/search/channel", controllers.SearchSubscriptions)
		v1.POST("/push/notifications", controllers.PushNotificationTest)
	}
	authorized := v1.Group("/")
	authorized.Use(middlewares.Auth())
	{
		authorized.GET("/profile", controllers.GetProfile)
	}
	return r
}
