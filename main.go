package main

import (
	"flag"
	"fmt"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/models"
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/notifications"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/config"
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/db"
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/routers"
)

func init() {
	flag.Parse()
}

func main() {
	fmt.Println("Scrapping...")
	db.Init()
	go models.Init()
	// controllers.InitSocialAuth()
	conf := config.Get()
	go notifications.Start(conf.SSEAddress)
	routers.Init().Run(conf.Port)
}
