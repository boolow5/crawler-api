package models

import (
	"fmt"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/config"
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/db"
	"golang.org/x/crypto/bcrypt"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"

	"gopkg.in/mgo.v2/bson"
)

// User is an object representing a user in the database
type User struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	Email    string `json:"email" valid:"required"`
	Password string `json:"password" valid:"required"`
	Status   int    `json:"status"`

	Profile Profile `json:"profile" bson:"profile"`

	Nationality string `json:"nationality" valid:"required"`

	Provider string `json:"provider"`

	Likes         int `bson:"topics_you_like" json:"topics_you_like"`
	AnswerCount   int `bson:"answer_count" json:"answer_count"`
	QuestionCount int `bson:"question_count" json:"question_count"`

	LastToken string `json:"last_token"`

	Timestamp `bson:",inline"`
}

// Profile is an object representing user's basic data that everyone can see
type Profile struct {
	UserID    bson.ObjectId `bson:"user_id" json:"user_id" `
	FirstName string        `bson:"first_name" json:"first_name" valid:"required"`
	LastName  string        `bson:"last_name" json:"last_name" valid:"required"`
	Gender    string        `bson:"gender" json:"gender" valid:"required"`
	AvatarURL string        `bson:"profile_picture" json:"profile_picture"`
}

// String converts user profile to full name
func (u *Profile) String() string {
	return fmt.Sprintf("%s %s", u.FirstName, u.LastName)
}

// HashPassword returns hashed password for user data security
func (u *User) HashPassword() (err error) {
	if len(u.Password) > 50 {
		return fmt.Errorf("Maximum password length is %d characters", 50)
	}
	u.Password, err = hashPassword(u.Password)
	if err != nil {
		return err
	}
	return nil
}

func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(hashedPassword), err
}

// Exists checks if user with current email exists in the system
func (u *User) Exists() (bool, error) {
	count, err := db.Count(ClUser, &bson.M{"email": u.Email})
	return count > 0, err
}

// Save stores current user to the database
func (u *User) Save() error {
	if len(u.Email) < 1 && len(u.Password) < 1 {
		return fmt.Errorf("%s and %s are required", "Email", "password")
	}

	if len(u.Password) < 51 {
		err := u.HashPassword()
		if err != nil {
			return err
		}
	}

	u.ID = bson.NewObjectId()
	u.Profile.UserID = u.ID

	err := db.Create(ClUser, u)
	if err != nil {
		return err
	}

	return nil
}

// GenerateToken converts user data to JWT token
func (u *User) GenerateToken(jwtKey string) error {
	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims = map[string]interface{}{
		"id":              u.ID.Hex(),
		"email":           u.Email,
		"first_name":      u.Profile.FirstName,
		"last_name":       u.Profile.LastName,
		"profile_picture": u.Profile.AvatarURL,
		"nationality":     u.Nationality,
		"provider":        u.Provider,
		"status":          u.Status,
		"gender":          u.Profile.Gender,
	}
	var err error
	u.LastToken, err = token.SignedString([]byte(jwtKey))
	return err
}

// Authenticate checks if passed un-hashed password is valid
func (u *User) Authenticate(pass string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(pass))
	return err == nil, err
}

// GetUserByEmail finds user by his/her email
func GetUserByEmail(email string) (User, error) {
	user := User{}
	err := db.FindOne(ClUser, &bson.M{"email": email}, &user)
	return user, err
}

// Response returns user without password field data
func (u *User) Response() *User {
	u.Password = ""
	u.GenerateToken(config.Get().JWTKey)
	return u
}

// GetUserByID finds user by his/her ID
func GetUserByID(userID bson.ObjectId) (User, error) {
	user := User{}
	err := db.FindOne(ClUser, &bson.M{"_id": userID}, &user)
	return user, err
}
