package models

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/db"
	"gopkg.in/mgo.v2/bson"
)

// Subscription represents a channel a user listens for notifications
type Subscription struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name      string        `json:"name" bson:"name"`
	Category  string        `json:"category" bson:"category"`
	Timestamp `bson:",inline"`
}

// Save stores subscription to the database
func (s *Subscription) Save() error {
	s.ID = bson.NewObjectId()
	now := time.Now()
	s.CreatedAt = now
	s.UpdatedAt = now

	s.Name = strings.ToLower(s.Name)
	s.Category = strings.ToLower(s.Category)

	err := db.Create(ClSubscription, s)
	if err != nil {
		fmt.Printf("[Error] %v\t%v\n", time.Now(), err)
		return err
	}
	return nil
}

// GetURL returns the current subscription channel URL
func (s Subscription) GetURL() string {
	if strings.TrimSpace(s.Category) == "" {
		return fmt.Sprintf("/channel/all/%s", s.Name)
	}
	return fmt.Sprintf("/channel/%s/%s", s.Category, s.Name)
}

// GetSubscriptions finds subscription channels from db
func GetSubscriptions() ([]Subscription, error) {
	var subs []Subscription
	err := db.FindAll(ClSubscription, &bson.M{}, &subs)
	return subs, err
}

// SearchSubscriptions finds subscription channels from db
func SearchSubscriptions(q, category string) ([]Subscription, error) {
	var subs []Subscription
	err := db.FindAll(ClSubscription, &bson.M{"name": bson.RegEx{Pattern: q, Options: "i"}, "category": category}, &subs)
	return subs, err
}
