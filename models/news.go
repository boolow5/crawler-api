package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/notifications"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/config"
	"bitbucket.org/boolow5/NewsCrawler/crawler-api/db"
	"github.com/PuerkitoBio/goquery"
	"gopkg.in/mgo.v2/bson"
)

var (
	// AllCategories categories of available websites
	AllCategories []string
	allWebsites   []Website
)

// Website is an object representing a website
type Website struct {
	ID                bson.ObjectId `json:"id" bson:"_id,omitempty"`
	URL               string        `json:"url" bson:"url"`
	Name              string        `json:"name" bson:"name"`
	Category          string        `json:"category" bson:"category"`
	Language          string        `json:"language" bson:"language"`
	NewsItems         []News        `json:"news_items" bson:"news_items"`
	BaseSelector      string        `json:"base_selector" bson:"base_selector"`
	ImageSelector     string        `json:"image_selector" bson:"image_selector"`
	DefaultImage      string        `json:"default_image" bson:"default_image"`
	ImageSelectorAttr string        `json:"image_selector_attr" bson:"image_selector_attr"`
	TitleSelector     string        `json:"title_selector" bson:"title_selector"`
	Timestamp         `bson:",inline"`
}

type WebsiteSummary struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	URL      string        `json:"url" bson:"url"`
	Name     string        `json:"name" bson:"name"`
	Category string        `json:"category" bson:"category"`
	Language string        `json:"language" bson:"language"`
}

// News is an object representing an article
type News struct {
	ID        bson.ObjectId  `json:"id" bson:"_id,omitempty"`
	URL       string         `json:"url" bson:"url"`
	Title     string         `json:"title" bson:"title"`
	ImageURL  string         `json:"image_url" bson:"image_url"`
	Website   WebsiteSummary `json:"website" bson:"website"`
	Views     int            `json:"views" bson:"views"`
	Timestamp `bson:",inline"`
}

// Init initializes all required data and configurations
func Init() {
	fmt.Println("news.Init()")
	fmt.Println(db.AddUniqueIndex(ClNewsItem, "url"))
	fmt.Println(db.AddUniqueIndex(ClWebsite, "url"))
	fmt.Println(db.AddUniqueIndex(ClSubscription, "name"))

	// err := db.FindAll(ClWebsite, &bson.M{}, &allWebsites)
	// if err != nil {
	// 	panic("Failed to find websites")
	// }

	allWebsites = laodSitesFromFile()
	// found := []string{}
	// for _, site := range allWebsites {
	// 	for _, site2 := range fileWebsites {
	// 		if site.URL == site2.URL {
	// 			found = append(found, site.URL)
	// 		}
	// 	}
	// }
	// if len(allWebsites) == 0 {
	// 	fmt.Printf("\n[Warning] %v\tNo website is found in the database.\n", time.Now())
	// 	for _, site := range fileWebsites {
	// 		fmt.Printf("[Warning] %v\tSaving %s to the database.\n", time.Now().Format(time.Stamp), site.Name)
	// 		site.Save()
	// 	}
	// } else {
	// 	fmt.Printf("\n[Debug] %v\tFound %d in database and %d in file.\n", time.Now().Format(time.Stamp), len(allWebsites), len(fileWebsites))
	// 	for _, site := range fileWebsites {
	// 		if utils.StrIndexOf(found, site.URL) < 0 {
	// 			fmt.Printf("\n%s\t", site.URL)
	// 			allWebsites = append(allWebsites, site)
	// 			site.Save()
	// 		}
	// 	}
	// }

	updateCategories()

	time.Sleep(time.Second * 10)

	delay := time.Duration(config.Get().ScrapperDelay) * time.Minute
	for {
		for i := 0; i < len(allWebsites); i++ {
			allWebsites[i].Visit()
			time.Sleep(1 * time.Second)
		}
		fmt.Println("___________________________________")
		fmt.Printf("[Done] %v\tVisiting %d websites. Will visit after %s again!\n", time.Now().Format(time.Stamp), len(allWebsites), delay)
		time.Sleep(delay)
	}
}

func laodSitesFromFile() []Website {
	sites := []Website{}
	filePath := ""
	if len(os.Args) > 1 {
		filePath = filepath.Join(os.Args[1], "websites.json")
	} else {
		exe, err := os.Executable()
		if err != nil {
			panic(err)
		}
		exePath := filepath.Dir(exe)
		fmt.Println("\nexePath\n:", exePath)
		filePath = filepath.Join(exePath, "data", "websites.json")
	}

	fileData, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(fileData, &sites)
	if err != nil {
		panic(err)
	}

	for i := 0; i < len(sites); i++ {
		fmt.Println("site:", sites[i].Name, "\tcategory:", sites[i].Category)
	}

	return sites
}

func updateCategories() []string {
	m := map[string]string{}
	for _, site := range allWebsites {
		m[site.Category] = site.Category
	}
	AllCategories = nil
	for k := range m {
		AllCategories = append(AllCategories, k)
	}
	return AllCategories
}

// GetAllCategories returns all available categories
func GetAllCategories() []string {
	if len(AllCategories) > 0 {
		return AllCategories
	}
	categories := updateCategories()
	fmt.Printf("[DEBUG] %s \n%d categories found\n", time.Now().Format(time.Stamp), len(AllCategories))
	return categories
}

// ToSummary hides other data
func (w Website) ToSummary() WebsiteSummary {
	return WebsiteSummary{
		ID:       w.ID,
		URL:      w.URL,
		Name:     w.Name,
		Category: w.Category,
		Language: w.Language,
	}
}

// Visit fetches data from website
func (w *Website) Visit() error {
	// fmt.Println("w.Visit()", w.URL)

	// save website if not saved before
	// w.Save()
	doc, err := goquery.NewDocument(w.URL)
	if err != nil {
		fmt.Println("Error fetching website. Reason:", err)
		return err
	}
	// if w.Name == "Baahin" || w.Name == "Puntland Times" {
	// 	fmt.Printf("DOC: %+v\n", doc.Find(w.BaseSelector))
	// }
	var currentItem *News
	doc.Find(w.BaseSelector).Each(func(i int, section *goquery.Selection) {
		// get the first 5 items
		if i > 5 {
			return
		}
		var part *goquery.Selection
		if len(w.TitleSelector) == 0 {
			part = section
		} else {
			part = section.Find(w.TitleSelector)
		}
		var imgSrc string
		if len(w.DefaultImage) > 0 && w.ImageSelector == "default" {
			imgSrc = w.DefaultImage
		} else if w.ImageSelector != "default" {
			img := section.Find(w.ImageSelector)
			if len(w.ImageSelectorAttr) > 0 {
				imgSrc, _ = img.Attr(w.ImageSelectorAttr)
			} else {
				imgSrc, _ = img.Attr("src")
			}
		}

		text := strings.TrimSpace(part.Text())
		link, ok := part.Attr("href")
		if !ok {
			part = part.Find("a")
			link, ok = part.Attr("href")
		}
		// w.Name == "Baahin"
		// if w.URL == "http://puntlandtimes.ca" || w.Name == "Puntland Times" {
		// 	fmt.Printf("[%s]: %s > %s\n", w.Name, w.BaseSelector, w.TitleSelector)
		// 	fmt.Printf("[%s]: \ntitle: %s\nlink: %s\nimg: %s\n", w.Name, text, link, imgSrc)
		// }

		if ok {
			fmt.Printf("%s", "_")
			currentItem = &News{
				Title:    text,
				URL:      link,
				ImageURL: imgSrc,
				Website:  w.ToSummary(),
			}

			if currentItem.Save() == nil {
				fmt.Printf("%s", "#")
			}
			time.Sleep(100 * time.Millisecond)
		}

	})
	fmt.Printf("\n[DONE] %v\t%s\n\n", time.Now().Format(time.Stamp), w.URL)
	return nil
}

// GetNewsItems fetches news articles from database
func GetNewsItems(page, limit int, category string) ([]News, error) {
	fmt.Printf("GetNewsItems page: %d, limit: %d, category: %s\n", page, limit, category)
	var items []News
	var err error
	beforeSevenDays := time.Now().Add(-24 * 7 * time.Hour)
	fmt.Printf("[DEBUG] %v\tbefore 7 days: %s \n", time.Now().Format(time.Stamp), beforeSevenDays.Format(time.Stamp))
	if category == "all" {
		err = db.FindPageWithLimit(ClNewsItem, &bson.M{
			"created_at": bson.M{"$gte": beforeSevenDays},
		}, page, limit, &items, "-created_at")
		fmt.Printf("GetNewsItems items:%d\n", len(items))
	} else {
		err = db.FindPageWithLimit(ClNewsItem, &bson.M{
			"created_at": bson.M{"$gte": beforeSevenDays},
			"website.category": bson.M{
				"$regex": bson.RegEx{Pattern: category, Options: "i"},
			},
		}, page, limit, &items, "-created_at")
	}

	return items, err
}

// SearchNewsItems finds news items by search q
func SearchNewsItems(page, limit int, category, q string) ([]News, error) {
	var items []News
	var err error
	if category == "all" {
		err = db.FindPageWithLimit(ClNewsItem, &bson.M{
			"$or": []bson.M{
				{"title": bson.RegEx{Pattern: q, Options: "i"}},
				{"url": bson.RegEx{Pattern: q, Options: "i"}},
			},
		}, page, limit, &items, "-created_at")
	} else {
		err = db.FindPageWithLimit(ClNewsItem, &bson.M{
			"website.category": bson.M{
				"$regex": bson.RegEx{Pattern: category, Options: "i"},
			},
			"$or": []bson.M{
				{"title": bson.RegEx{Pattern: q, Options: "i"}},
				{"url": bson.RegEx{Pattern: q, Options: "i"}},
			},
		}, page, limit, &items, "-created_at")
	}
	return items, err
}

// Save saves website in the database
func (w *Website) Save() error {
	// fmt.Println("website.Save()")
	w.ID = bson.NewObjectId()
	now := time.Now()
	w.CreatedAt = now
	w.UpdatedAt = now

	err := db.Create(ClWebsite, w)
	if err != nil {
		if db.IsDupError(err) {
			return err
		}
		fmt.Printf("[Error] %v\t%v\n", time.Now(), err)
		return err
	}
	return nil
}

// Save stores news item in the database
func (n *News) Save() error {
	// fmt.Println("news.Save()")
	n.ID = bson.NewObjectId()
	now := time.Now()
	n.CreatedAt = now
	n.UpdatedAt = now

	err := db.Create(ClNewsItem, n)
	if err != nil {
		if db.IsDupError(err) {
			return err
		}
		fmt.Printf("[Error] %v\t%v\n", time.Now(), err)
		return err
	}
	notifications.SendSSE("news", n, fmt.Sprintf("/news/%s", n.Website.Category))
	return nil
}
func shorten(str string, max int) string {
	if len(str) <= max {
		return str
	}
	return str[:max] + "..."
}
