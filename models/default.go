package models

import (
	"time"

	"bitbucket.org/boolow5/NewsCrawler/crawler-api/db"
	"gopkg.in/mgo.v2/bson"
)

const (
	// ClUser collection name
	ClUser = "user"
	// ClWebsite collection name
	ClWebsite = "website"
	// ClNewsItem collection name
	ClNewsItem = "news_item"
	// ClSubscription collection name
	ClSubscription = "subscription"
)

// Version number
var (
	Version      string
	CommitNo     string
	LastBuidDate string
)

// Timestamp represents embedded object for update, delete and create timestamps
type Timestamp struct {
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
	DeletedAt time.Time `json:"-" bson:"deleted_at"`
}

// CountItems counts items in the colName using query as filter
func CountItems(colName string, query bson.M) int {
	if colName == "" {
		return -1
	}
	i, err := db.Count(colName, &query)
	if err != nil {
		return -1
	}
	return i
}
